import initialState from './initial-state';

export default {
  SET_USER(state, payload) {
    state.profile = payload;
  },

  UPDATE_USER(state, payload) {
    state.profile = {
      ...state.profile,
      ...payload,
    };
  },

  SET_AVATAR_SAVING_PROCESS(state, payload) {
    state.isAvatarSaving = payload;
  },

  SET_PROFILE_UPDATING_PROCESS(state, payload) {
    state.isProfileUpdating = payload;
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
