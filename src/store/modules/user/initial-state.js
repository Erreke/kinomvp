export default {
  profile: null,
  isAvatarSaving: false,
  isProfileUpdating: false,
};
