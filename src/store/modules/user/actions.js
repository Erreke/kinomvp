import userAPI from '@/api/user';
import authAPI from '@/api/auth';

function composeUserProfile(user, userData) {
  return {
    // data from Firebase.Auth
    uid: user.uid,
    email: user.email,
    phone: user.phoneNumber,
    emailVerified: user.emailVerified,
    displayName: user.displayName,
    isAnonymous: user.isAnonymous,
    creationTime: user.metadata.creationTime,
    lastSignInTime: user.metadata.lastSignInTime,
    phoneNumber: user.phoneNumber,
    photoURL: user.photoURL,
    providerData: user.providerData,

    // data from Firestore 'profiles' table
    ...userData,
  };
}

export default {
  INIT_USER({ commit }, user) {
    return userAPI
      .fetchUserProfile(user.uid)
      .then((userData) => {
        const userProfile = composeUserProfile(user, userData);

        commit('SET_USER', userProfile);
        commit('auth/sign-in/SET_PROCESSING', false, { root: true });

        return userProfile;
      });
  },

  SAVE_USER_INFO({ commit }, {
    uid, fields, values, loader,
  }) {
    if (loader) {
      commit(loader, true, { root: true });
    }

    return userAPI.saveUserInfo(uid, fields, values)
      .then(() => {
        if (loader) {
          commit(loader, false, { root: true });
        }

        return true;
      });
  },

  UPDATE_PROFILE({ commit }, profile) {
    commit('SET_PROFILE_UPDATING_PROCESS', true);

    return authAPI.updateProfile(profile)
      .then(() => {
        commit('SET_PROFILE_UPDATING_PROCESS', false);
      });
  },
};
