import state from './state';
import mutations from './mutations';
import getters from './getters';
import moviesModule from './modules/movies';
import seriesModule from './modules/series';
import documentariesModule from './modules/documentaries';
import cartoonsModule from './modules/cartoons';
import collectionsModule from './modules/collections';
// import personsModule from './modules/persons';

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  modules: {
    'movies': moviesModule,
    'series': seriesModule,
    'documentaries': documentariesModule,
    'cartoons': cartoonsModule,
    'collections': collectionsModule,
    // 'persons': personsModule,
  },
};
