import contentAPI from '@/api/content';
import actionsPrototype from '../_proto/_actions';

export default {
  ...actionsPrototype('series'),

  async FETCH_SINGLE({ state, commit }, id) {
    // console.log('FETCH_SINGLE');

    try {
      commit('SET_SINGLE_FETCHING_PROCESS', true);

      let result = state.all.items[id];

      if (!result) {
        result = await contentAPI.fetchSingle('series', id);
      }

      if (!result.episodes) {
        const episodes = await contentAPI.fetchEpisodes(id);
        const sortedEpisodes = [];

        episodes.sort((itemA, itemB) => {
          const a = itemA.episode * 10 + itemA.season * 100;
          const b = itemB.episode * 10 + itemB.season * 100;

          return a - b;
        }).forEach(item => {
          if (!sortedEpisodes[item.season - 1]) {
            sortedEpisodes.push([]);
          }

          sortedEpisodes[item.season - 1].push(item);
        });

        result.episodes = sortedEpisodes;
      }

      commit('SET_SINGLE_RESULT', result);
      commit('SET_SINGLE_FETCHING_PROCESS', false);

    } catch (error) {
      console.log('error.message', error.message);
      console.log('error.code', error.code);
      console.log('error', error);

      commit('SET_SINGLE_FETCHING_PROCESS', false);
      commit('SET_SINGLE_FETCHING_ERROR', error);
    }
  },
};
