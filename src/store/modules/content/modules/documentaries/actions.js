import actionsPrototype from '../_proto/_actions';

export default {
  ...actionsPrototype('documentaries'),
};
