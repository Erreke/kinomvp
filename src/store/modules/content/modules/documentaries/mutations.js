import mutationsPrototype from '../_proto/_mutations';

export default {
  ...mutationsPrototype(),
};
