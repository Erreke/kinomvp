import contentAPI from '@/api/content';
import actionsPrototype from '../_proto/_actions';

export default {
  ...actionsPrototype('collections'),

  async FETCH_SINGLE({ state, commit, rootState }, id) {
    // console.log('FETCH_SINGLE 2');

    try {
      commit('SET_SINGLE_FETCHING_PROCESS', true);

      let collection = state.all.items[id];
      let collectionItems = [];
      let unexistItemIds = [];
      let itemsFromServer = [];

      if (!collection) {
        collection = await contentAPI.fetchSingle('collections', id);
      }

      if(collection.items && Array.isArray(collection.items)) {
        collection.items.forEach(itemId => {
          if (typeof itemId === 'string') {
            const collectionItem = rootState.content[collection.type][itemId];

            if(collectionItem) {
              collectionItems.push(collectionItem);
            } else {
              unexistItemIds.push(itemId);
            }
          } else {
            collectionItems.push(itemId);
          }
        });

        if (unexistItemIds.length) {
          itemsFromServer = await contentAPI.fetchSome(collection.type, collection.items);
        }

        collectionItems = collectionItems.concat(Object.values(itemsFromServer));
      }

      collection.items = collectionItems;

      // console.log('collection:', collection);

      commit('SET_SINGLE_RESULT', collection);
      commit('SET_SINGLE_FETCHING_PROCESS', false);
    } catch (error) {
      console.log('error.message', error.message);
      console.log('error.code', error.code);
      console.log('error', error);

      commit('SET_SINGLE_FETCHING_PROCESS', false);
      commit('SET_SINGLE_FETCHING_ERROR', error)
    }
  },
};
