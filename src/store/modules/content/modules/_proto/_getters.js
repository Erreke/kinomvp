export function latest() {
  return function (state) {
    return Object
      .values(state.all.items)
      .sort((itemA, itemB) => itemB.createdAt?.seconds - itemA.createdAt?.seconds)
      .slice(0,5); 
  }
}
