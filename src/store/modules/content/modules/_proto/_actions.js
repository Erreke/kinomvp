import contentAPI from '@/api/content';

export default function actionsPrototype(contentType) {
  return {
    FETCH_ALL({ state, commit }) {
      // console.log('FETCH_ALL contentType:', contentType);

      commit('SET_ALL_FETCHING_PROCESS', true);
  
      if (Object.keys(state.all.items).length) {
        commit('SET_ALL_FETCHING_PROCESS', false);

        // console.log('JUST PROMISE.RESOLVE:', contentType);

        return Promise.resolve();
      }
  
      return contentAPI
        .fetchAll(contentType)
        .then((response) => {
          // console.log('FETCH_ALL then contentType:', contentType);
          // console.log('FETCH_ALL then response:', response);

          commit('SET_ALL_RESULT', response);
          commit('SET_ALL_FETCHING_PROCESS', false);
        })
        .catch((error) => {
          commit('SET_ALL_FETCHING_PROCESS', false);
          commit('SET_ALL_FETCHING_ERROR', error);
        });
    },
  
    FETCH_SINGLE({ state, commit }, id) {
      // console.log('FETCH_SINGLE');

      commit('SET_SINGLE_FETCHING_PROCESS', true);
  
      if (state.all.items[id]) {
        commit('SET_SINGLE_RESULT', Object.assign({}, state.all.items[id]));
        commit('SET_SINGLE_FETCHING_PROCESS', false);

        // console.log('JUST PROMISE.RESOLVE:', contentType);
  
        return Promise.resolve();
      }
  
      return contentAPI
        .fetchSingle(contentType, id)
        .then((response) => {
          commit('SET_SINGLE_RESULT', response);
          commit('SET_SINGLE_FETCHING_PROCESS', false);
        })
        .catch((error) => {
          commit('SET_SINGLE_FETCHING_PROCESS', false);
          commit('SET_SINGLE_FETCHING_ERROR', error);
        });
    },
  };
}
