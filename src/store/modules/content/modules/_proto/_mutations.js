export default function mutationsPrototype() {
  return {
    SET_ALL_FETCHING_PROCESS(state, payload) {
      state.all.isFetchingProcess = payload;
    },
  
    SET_ALL_RESULT(state, payload) {
      state.all.items = payload;
    },
  
    SET_ALL_FETCHING_ERROR(state, payload) {
      state.all.fetchingError = payload;
    },
  
    SET_SINGLE_FETCHING_PROCESS(state, payload) {
      state.current.isFetchingProcess = payload;
    },
  
    SET_SINGLE_RESULT(state, payload) {
      state.current.item = payload;
    },
  
    SET_SINGLE_FETCHING_ERROR(state, payload) {
      state.current.fetchingError = payload;
    },
  
    RESET_STATE(state) {
      state.all = {
        items: {},
        isFetchingProcess: false,
        fetchingError: null,
      }
  
      state.current = {
        item: {},
        isFetchingProcess: false,
        fetchingError: null,
      };
    },
  }
}
