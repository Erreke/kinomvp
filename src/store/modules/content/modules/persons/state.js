export default {
  all: {
    items: {},
    isFetchingProcess: false,
    fetchingError: null,
  },
  current: {
    item: {},
    isFetchingProcess: false,
    fetchingError: null,
  },
};
