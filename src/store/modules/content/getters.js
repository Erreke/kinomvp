import ratingFormat from '@/utils/filters/rating-format';
class ContentHandler {
  constructor(items = [], query = {}) {
    this.items = items;
    this.query = query;
    this.result = this.items;
  }

  filterByTitles() {
    const query = this.query.q;

    if (query && query !== '') {
      console.log('filterByTitles query:', query);

      this.result = this.items.filter((content) => {
        return content.title?.toLowerCase().includes(query.toLowerCase())
          || content.originalTitle?.toLowerCase().includes(query.toLowerCase())
          || content.firstName?.toLowerCase().includes(query.toLowerCase())
          || content.secondName?.toLowerCase().includes(query.toLowerCase());
      });
    }

    return this;
  }

  filterByCountries() {
    if (this.query.countries) {
      const countries = this.query.countries.split('|');

      console.log('filterByCountries countries:', countries);

      if (countries.length) {
        this.result = this.result.filter((content) => {
          if (content.countries && Array.isArray(content.countries)) {
            return content.countries.some(item => countries.includes(item));
          }

          return false;
        });
      }
    }

    return this;
  }

  filterByYears() {
    if (this.query.years) {
      const years = this.query.years.split('|');

      console.log('filterByYears years:', years);

      if (years.length) {
        const yearsRange = new Set();

        years.forEach(year => {
          if (year.includes('-')) {
            const range = year.split('-');
            const minYear = Math.min(...range);
            const maxYear = Math.max(...range);

            for (let i = minYear; i < maxYear; i += 1) {
              yearsRange.add(i);
            }
          }

          yearsRange.add(parseInt(year));
        });

        this.result = this.result.filter((content) => yearsRange.has(content.year));
      }
    }

    return this;
  }

  filterByGenres() {
    if (this.query.genres) {
      const genres = this.query.genres.split('|');

      console.log('filterByGenres genres:', genres);

      if (genres.length) {
        this.result = this.result.filter((content) => {
          if (content.genres && Array.isArray(content.genres)) {
            return content.genres.some(item => genres.includes(item));
          }

          return false;
        });
      }
    }

    return this;
  }

  filterByTranslations() {
    if (this.query.translations) {
      const translations = this.query.translations.split('|');

      console.log('filterByTranslations translations:', translations);

      if (translations.length) {
        this.result = this.result.filter((content) => {
          if (content.translations && Array.isArray(content.translations)) {
            return content.translations.some(item => translations.includes(item));
          }

          return false;
        });
      }
    }

    return this;
  }

  filterByResolutions() {
    if (this.query.resolutions) {
      const resolutions = this.query.resolutions.split('|');

      console.log('filterByResolutions resolutions:', resolutions);

      if (resolutions.length) {
        this.result = this.result.filter((content) => {
          if (content.resolutions && Array.isArray(content.resolutions)) {
            return content.resolutions.some(item => resolutions.includes(item));
          }

          return false;
        });
      }
    }

    return this;
  }

  filterByCertificate() {
    if (this.query.certificates) {
      const certificates = this.query.certificates.split('|');

      console.log('filterByCertificate certificates:', certificates);
  
      if (certificates.length) {
        if (certificates.includes('all')) return this;
  
        this.result = this.result.filter((content) => {
          const age = parseInt(content.certificate);
  
          return certificates.some(item => {
            return parseInt(item) >= age;
          });
        });
      }
    }

    return this;
  }

  filterByRatings() {
    const ownMin = Number(this.query['rating-own-min'] || 0) / 10;
    const ownMax = Number(this.query['rating-own-max'] || 100) / 10;

    const imdbMin = Number(this.query['rating-imdb-min'] || 0) / 10;
    const imdbMax = Number(this.query['rating-imdb-max'] || 100) / 10;

    const rottentomatoesMin = Number(this.query['rating-rottentomatoes-min'] || 0) / 10;
    const rottentomatoesMax = Number(this.query['rating-rottentomatoes-max'] || 100) / 10;

    const metacriticMin = Number(this.query['rating-metacritic-min'] || 0) / 10;
    const metacriticMax = Number(this.query['rating-metacritic-max'] || 100) / 10;

    const kinopoiskMin = Number(this.query['rating-kinopoisk-min'] || 0) / 10;
    const kinopoiskMax = Number(this.query['rating-kinopoisk-max'] || 100) / 10;

    if (ownMin > 0 || ownMax < 10) {
      this.result = this.result.filter((content) => {
        if (content.ratingOwn) {
          return content.ratingOwn >= ownMin && content.ratingOwn <= ownMax;
        }

        return true;
      });
    }

    if (imdbMin > 0 || imdbMax < 100) {
      this.result = this.result.filter((content) => {
        if (content.ratingImdb) {
          return content.ratingImdb >= imdbMin && content.ratingImdb <= imdbMax;
        }

        return true;
      });
    }

    if (rottentomatoesMin > 0 || rottentomatoesMax < 100) {
      this.result = this.result.filter((content) => {
        if (content.ratingRottentomatoes) {
          return content.ratingRottentomatoes >= rottentomatoesMin && content.ratingRottentomatoes <= rottentomatoesMax ;
        }

        return true;
      });
    }

    if (metacriticMin > 0 || metacriticMax < 100) {
      this.result = this.result.filter((content) => {
        if (content.ratingMetacritic) {
          return content.ratingMetacritic >= metacriticMin && content.ratingMetacritic <= metacriticMax ;
        }

        return true;
      });
    }

    if (kinopoiskMin > 0 || kinopoiskMax < 100) {
      this.result = this.result.filter((content) => {
        if (content.ratingKinopoisk) {
          return content.ratingKinopoisk >= kinopoiskMin && content.ratingKinopoisk <= kinopoiskMax ;
        }

        return true;
      });
    }

    return this;
  }

  ratingSort(items, direction) {
    return items.sort((itemA, itemB) => {
      const a = ratingFormat(itemA);
      const b = ratingFormat(itemB);

      if (direction === "asc") {
        return b - a;
      }

      return a - b;
    });
  }

  numberSort(items, field, direction) {
    return items.sort((itemA, itemB) => {
      if (direction === "asc") {
        return itemB[field] - itemA[field];
      }

      return itemA[field] - itemB[field];
    });
  }

  textSort(items, field, direction) {
    return items.sort((itemA, itemB) => {

      const a = itemA[field] && Array.isArray(itemA[field]) && itemA[field][0].toLowerCase() || 0;
      const b = itemB[field] && Array.isArray(itemB[field]) && itemB[field][0].toLowerCase() || 0;

      if (direction === "asc") {
        if (a < b) {
          return -1;
        } else if (a > b) {
          return 1;
        }

        return 0;
      } else {
        if (a > b) {
          return -1;
        } else if (a < b) {
          return 1;
        }

        return 0;
      }
    });
  }

  sort() {
    const defaultSortBy = 'year';
    const defaultSortDirection = 'asc';

    if (this.query.sort) {
      const sortByTypes = ['countries', 'year', 'genres', 'translations', 'resolutions', 'certificate', 'rating'];
      const sortDirectionTypes = ['asc', 'desc'];

      let [sortBy, sortDirection] = this.query.sort.split('-');

      sortBy = sortByTypes.includes(sortBy) ? sortBy : defaultSortBy;
      sortDirection = sortDirectionTypes.includes(sortDirection) ? sortDirection : defaultSortDirection;

      if (sortBy === 'rating') {
        this.result = this.ratingSort(this.result, sortDirection);
      } else if (sortBy === 'year') {
        this.result = this.numberSort(this.result, sortBy, sortDirection);
      } else {
        this.result = this.textSort(this.result, sortBy, sortDirection);
      }
    } else {
      this.result = this.numberSort(this.result, defaultSortBy, defaultSortDirection);
    }

    return this;
  }

  done() {
    return this.result;
  }
}

function advancedSearchResults(items, query) {
  const contentHandler = new ContentHandler(items, query);

  return contentHandler
    .filterByTitles()
    .filterByCountries()
    .filterByYears()
    .filterByGenres()
    .filterByTranslations()
    .filterByResolutions()
    .filterByCertificate()
    .filterByRatings()
    .sort()
    .done();
}

function getRaingFiltersCount(query) {
  let result = 0;

  if (query['rating-own-min'] && query['rating-own-min'] > 0) {
    result += 1;
  }

  if (query['rating-imdb-min'] && query['rating-imdb-min'] > 0) {
    result += 1;
  }

  if (query['rating-rottentomatoes-min'] && query['rating-rottentomatoes-min'] > 0) {
    result += 1;
  }

  if (query['rating-metacritic-min'] && query['rating-metacritic-min'] > 0) {
    result += 1;
  }

  if (query['rating-kinopoisk-min'] && query['rating-kinopoisk-min'] > 0) {
    result += 1;
  }

  if (query['rating-own-max'] && query['rating-own-max'] < 100) {
    result += 1;
  }

  if (query['rating-imdb-max'] && query['rating-imdb-max'] < 100) {
    result += 1;
  }

  if (query['rating-rottentomatoes-max'] && query['rating-rottentomatoes-max'] < 100) {
    result += 1;
  }

  if (query['rating-metacritic-max'] && query['rating-metacritic-max'] < 100) {
    result += 1;
  }

  if (query['rating-kinopoisk-max'] && query['rating-kinopoisk-max'] < 100) {
    result += 1;
  }

  return result;
}

export default {
  simpleSearchResults(state, getters, rootState) {
    const items = [
      ...Object.values(rootState.content.movies.all.items),
      ...Object.values(rootState.content.series.all.items),
      ...Object.values(rootState.content.documentaries.all.items),
      ...Object.values(rootState.content.cartoons.all.items),
    ];

    const contentHandler = new ContentHandler(items, rootState.route.query);

    return contentHandler
      .filterByTitles()
      .done();
  },

  moviesAdvancedSearchResults(state, getters, rootState) {
    return advancedSearchResults(Object.values(rootState.content.movies.all.items), rootState.route.query);
  },

  seriesAdvancedSearchResults(state, getters, rootState) {
    return advancedSearchResults(Object.values(rootState.content.series.all.items), rootState.route.query);
  },

  documentariesAdvancedSearchResults(state, getters, rootState) {
    return advancedSearchResults(Object.values(rootState.content.documentaries.all.items), rootState.route.query);
  },

  cartoonsAdvancedSearchResults(state, getters, rootState) {
    return advancedSearchResults(Object.values(rootState.content.cartoons.all.items), rootState.route.query);
  },

  collectionsAdvancedSearchResults(state, getters, rootState) {
    return advancedSearchResults(Object.values(rootState.content.collections.all.items), rootState.route.query);
  },

  // personsAdvancedSearchResults(state, getters, rootState) {
  //   return advancedSearchResults(Object.values(rootState.content.persons.all.items), rootState.route.query);
  // },

  advancedSearchResultsCount(state, getters) {
    return getters.moviesAdvancedSearchResults.length
     + getters.seriesAdvancedSearchResults.length
     + getters.documentariesAdvancedSearchResults.length
     + getters.cartoonsAdvancedSearchResults.length
     + getters.collectionsAdvancedSearchResults.length;
    //  + getters.personsAdvancedSearchResults.length;
  },

  activeFiltersCount(state, getters, rootState) {
    let countries = 0,
      years = 0,
      genres = 0,
      translations = 0,
      resolutions = 0,
      certificates = 0,
      rating = 0;

    if (rootState.route.query) {
      countries = rootState.route.query.countries ? rootState.route.query.countries.split('|').length : 0;
      years = rootState.route.query.years ? rootState.route.query.years.split('|').length : 0;
      genres = rootState.route.query.genres ? rootState.route.query.genres.split('|').length : 0;
      translations = rootState.route.query.translations ? rootState.route.query.translations.split('|').length : 0;
      resolutions = rootState.route.query.resolutions ? rootState.route.query.resolutions.split('|').length : 0;
      certificates = rootState.route.query.certificates ? rootState.route.query.certificates.split('|').length : 0;
      rating = getRaingFiltersCount(rootState.route.query);
    }

    return [
      countries,
      years,
      genres,
      translations,
      resolutions,
      certificates,
      rating,
    ];
  }
}