export default {
  filters: {
    query: '',
    countries: [],
    years: [],
    genres: [],
    translations: [],
    resolutions: [],
    certificates: [],
    ratings: {
      own: {
        min: 0,
        max: 100,
      },
      imdb: {
        min: 0,
        max: 100,
      },
      kp: {
        min: 0,
        max: 100,
      },
    },
  },
};
