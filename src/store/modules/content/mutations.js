import initialState from './initial-state';

export default {
  ADD_VALUE_TO_FILTER(state, { key, value }) {
    state.filters[key].push(value);
  },

  SET_FILTER_VALUES(state, { key, value }) {
    state.filters[key] = value;
  },

  REMOVE_VALUE_FROM_FILTER(state, { key, value }) {
    state.filters[key] = state.filters[key].filter((item) => {
      return item !== value;
    });
  },

  CHANGE_RATING_FILTER_VALUE(state, { key, field, value }) {
    state.filters.ratings[key][field] = value;
  },

  RESET_FILTERS(state) {
    state.filters = {
      query: '',
      countries: [],
      years: [],
      genres: [],
      translations: [],
      resolutions: [],
      certificates: [],
      ratings: {
        own: {
          min: 0,
          max: 100,
        },
        imdb: {
          min: 0,
          max: 100,
        },
        kp: {
          min: 0,
          max: 100,
        },
      },
    };
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
