import initialState from './initial-state';

export default {
  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
