import initialState from './initial-state';

export default {
  REGISTER_POPUP(state, id) {
    // console.log('REGISTER_POPUP');

    state.items[id] = {
      isActive: false,
    };
  },

  UNREGISTER_POPUP(state, id) {
    // console.log('UNREGISTER_POPUP');

    delete state.items[id];
  },

  CLOSE_ALL_POPUPS(state) {
    // console.log('CLOSE_ALL_POPUPS');

    Object.keys(state.items).map((key) => {
      state.items[key].isActive = false;
    });
  },

  OPEN_POPUP(state, id) {
    state.items[id].isActive = true;
  },

  CLOSE_POPUP(state, id) {
    state.items[id].isActive = false;
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
