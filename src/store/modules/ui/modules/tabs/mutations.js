import initialState from './initial-state';

export default {
  REGISTER_TAB(state, payload) {
    state.items[payload.uuid] = {
      activeTabId: payload.activeTabId,
    };
  },

  UNREGISTER_TAB(state, payload) {
    delete state.items[payload.uuid];
  },

  SET_ACTIVE_TAB(state, payload) {
    state.items[payload.uuid].activeTabId = payload.id;
  },

  RESET_STATE(state) {
    Object.assign(state, initialState);
  },
};
