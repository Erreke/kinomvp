import state from './state';
import mutations from './mutations';
import tabsModule from './modules/tabs';
import popupsModule from './modules/popups';

export default {
  namespaced: true,
  state,
  mutations,
  modules: {
    'tabs': tabsModule,
    'popups': popupsModule,
  },
};
