import authAPI from '@/api/auth';

export default {
  CHANGE_USER_EMAIL({ dispatch, commit }, { currentPassword, email }) {
    commit('CLEAR_ALL_ERRORS');
    commit('SET_PROCESSING', true);

    return authAPI.changeUserEmail(currentPassword, email)
      .then(() => dispatch('SEND_VERIFICATION_CODE_TO_NEW_EMAIL'))
      .catch((error) => {
        commit('SET_PROCESSING', false);
        commit('PUSH_ERROR', error.code.replace('auth/', ''));

        throw new Error('Unable to change user email');
      });
  },

  SEND_VERIFICATION_CODE_TO_NEW_EMAIL({ commit }) {
    return authAPI.sendEmailVerification()
      .then(() => {
        commit('SET_RESULT', true);
        commit('SET_PROCESSING', false);

        return true;
      })
      .catch((error) => {
        commit('SET_PROCESSING', false);
        commit('PUSH_ERROR', error.code.replace('auth/', ''));

        throw new Error('Unable to send verification email');
      });
  },
};
