import protoMutations from '@/store/modules/auth/_proto/mutations';
import protoState from '@/store/modules/auth/_proto/state';

export default {
  ...protoMutations(protoState()),
};
