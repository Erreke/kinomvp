import protoState from '@/store/modules/auth/_proto/state';

export default {
  ...protoState(),
};
