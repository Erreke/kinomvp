import authAPI from '@/api/auth';

export default {
  SIGN_UP({ commit }, { email, password }) {
    commit('SET_PROCESSING', true);

    commit('auth/change-email/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/change-password/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/change-phone/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/email-verification/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/recover-email/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/recover-password/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/reset-password/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/sign-in/CLEAR_ALL_ERRORS', null, { root: true });
    commit('auth/sign-up/CLEAR_ALL_ERRORS', null, { root: true });

    commit('auth/change-email/SET_RESULT', false, { root: true });
    commit('auth/change-password/SET_RESULT', false, { root: true });
    commit('auth/change-phone/SET_RESULT', false, { root: true });
    commit('auth/email-verification/SET_RESULT', false, { root: true });
    commit('auth/recover-email/SET_RESULT', false, { root: true });
    commit('auth/recover-password/SET_RESULT', false, { root: true });
    commit('auth/reset-password/SET_RESULT', false, { root: true });
    commit('auth/sign-in/SET_RESULT', false, { root: true });
    commit('auth/sign-up/SET_RESULT', false, { root: true });

    return authAPI.signUp(email, password)
      .then(() => {
        commit('SET_PROCESSING', false);
        commit('SET_RESULT', true);
      })
      .catch((error) => {
        commit('SET_PROCESSING', false);
        commit('MERGE_ERRORS', error);
      });
  },
};
