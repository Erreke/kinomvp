export default {
  firstFormErrors: [],
  secondFormErrors: [],
  isFirstFormProcessing: false,
  isSecondFormProcessing: false,
  firstFormResult: null,
  secondFormResult: null,
  tempNewPhoneNumber: null,
};
