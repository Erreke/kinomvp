import protoState from '@/store/modules/auth/_proto/state';
import initialState from './initial-state';

export default Object.assign({}, protoState(), initialState);
