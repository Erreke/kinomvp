import protoMutations from '@/store/modules/auth/_proto/mutations';
import protoState from '@/store/modules/auth/_proto/state';
import initialState from './initial-state';

export default {
  ...protoMutations({...protoState, ...initialState}),

  SET_CODE_SENDING_RESULT(state, payload) {
    state.isCodeSended = payload;
  },
};
