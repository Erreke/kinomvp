import authAPI from '@/api/auth';

export default {
  SIGN_OUT({ dispatch, commit }) {
    return authAPI.signOut()
      .then(() => {
        commit('SET_SING_OUTING_STATUS', true);

        return dispatch('RESET_STATE', null, { root: true })
      })
      .then(() => {
        commit('SET_SING_OUTING_STATUS', false);
      })
      .catch(() => {
        commit('SET_SING_OUTING_STATUS', false);
      });
  },
};
