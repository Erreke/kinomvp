import { createStore } from 'vuex'
import moduleUI from './modules/ui';
import moduleAuth from './modules/auth';
import moduleUser from './modules/user';
import moduleContent from './modules/content';

export default createStore({
  modules: {
    ui: moduleUI,
    auth: moduleAuth,
    user: moduleUser,
    content: moduleContent,
  },
  actions: {
    RESET_STATE: ({ commit }) => {
      commit('auth/RESET_STATE', null, { root: true });
      commit('user/RESET_STATE', null, { root: true });
    },
  },
})
