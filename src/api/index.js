import axios from 'axios';
import { HOST } from '@/config';

function getBeneficiarByEmail(email) {
  return axios.get(`${HOST}/api/v1/get-beneficiar-by-email/`, { params: { email } });
}

function sendVerificationCode(payload) {
  return axios.post(`${HOST}/api/v1/send-verification-code/`, payload);
}

export default {
  getBeneficiarByEmail,
  sendVerificationCode,
};
