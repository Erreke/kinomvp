import db, { fieldPath } from '@/utils/firebase/init';

function fetchAll(type) {
  // console.log('api fetchAll type:', type);

  return db
    .collection(type)
    .get()
    .then((snapshot) => {
      // console.log('snapshot.metadata:', snapshot.metadata);

      if (!snapshot.empty) {
        const content = {};

        snapshot.forEach((doc) => {
          content[doc.id] = {
            id: doc.id,
            ...doc.data(),
          };
        });

        return content;
      }
      
      throw new Error(`Unable fetch all data from ${type}`);
    });
}

function fetchSingle(type, id) {
  console.log('api fetchSingle type:', type);
  console.log('api fetchSingle id:', id);

  return db
    .collection(type)
    .doc(id)
    .get()
    .then((doc) => {
      // console.log('doc.metadata:', doc.metadata);

      if (doc.exists) {
        return {
          id,
          ...doc.data(),
        };
      }

      throw new Error(`Unable fetch single data from ${type} by id ${id}`);
    });
}

function fetchSome(type, ids) {
  console.log('api fetchSome type:', type);
  console.log('api fetchSome ids:', ids);

  return db
    .collection(type)
    .where(fieldPath.documentId(), 'in', ids)
    .get()
    .then((snapshot) => {
      // console.log('snapshot.metadata:', snapshot.metadata);

      if (!snapshot.empty) {
        const content = {};

        snapshot.forEach((doc) => {
          content[doc.id] = {
            id: doc.id,
            ...doc.data(),
          };
        });

        return content;
      }
      
      throw new Error(`Unable fetch some data from ${type}`);
    });
}

function fetchEpisodes(sid) {
  console.log('api fetchEpisodes sid:', sid);

  return db
    .collection('episodes')
    .where('sid', '==', sid)
    .get()
    .then((snapshot) => {
      // console.log('snapshot.metadata:', snapshot.metadata);

      if (!snapshot.empty) {
        const episodes = [];

        snapshot.forEach((doc) => {
          episodes.push(doc.data());
        });

        return episodes;
      }
      
      throw new Error(`Unable fetch data from Episodes`);
    });
}

export default {
  fetchAll,
  fetchSingle,
  fetchSome,
  fetchEpisodes,
};
