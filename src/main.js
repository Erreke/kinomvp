import { createApp } from 'vue';
// import VueMeta from 'vue-3-meta';
import firebase from 'firebase/app';
import 'firebase/auth';
import { sync } from 'vuex-router-sync';

import { CONTENT_ROUTES } from '@/config';

import store from '@/store';
import router from '@/router';
import i18n from '@/i18n';
import App from '@/App.vue';

import globalMixin from '@/utils/mixins/global';
import date from '@/utils/filters/date-format';
import dateTime from '@/utils/filters/date-time-format';
import duration from '@/utils/filters/duration-format';
import currency from '@/utils/filters/currency-format';
import number from '@/utils/filters/number-format';
import country from '@/utils/filters/country-format';
import genre from '@/utils/filters/genre-format';
import rating from '@/utils/filters/rating-format';
import resolution from '@/utils/filters/resolution-format';
import translation from '@/utils/filters/translation-format';
import certificate from '@/utils/filters/certificate';
import capitalize from '@/utils/filters/capitalize';

import clickOutside from '@/utils/directives/click-outside';

import '@/utils/firebase/init';

sync(store, router);

let isRouterHooksInited = false;
let app;

async function tryToAuth(user) {
  if (user) {
    await store.dispatch('user/INIT_USER', user);
  }
}

function setTheme(themeName) {
  localStorage.setItem('theme', themeName);
  document.documentElement.className = themeName;
}

function initTheme() {
  const themeFromStorage = localStorage.getItem('theme');
  const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: light)');

  if (themeFromStorage === null) {
    if (prefersDarkScheme.matches) {
      setTheme('theme-light');
    } else {
      setTheme('theme-dark');
    }
  } else {
    setTheme(themeFromStorage);
  }
}

function setMetaTitle(matched) {
  matched.forEach(m => {
    document.title = i18n.global.t(`page.${m.name}.meta-title`);
  });
}

function addValueToContentFilter({ key, values }) {
  values.forEach((value) => {
    store.commit('content/SET_FILTER_VALUES', { key, value });
  });
}

function setContentFilters({ countries, years, genres, translations, resolutions, certificates, ratings }) {
  if (countries) {
    addValueToContentFilter({
      key: 'countries',
      values: countries.split('|'),
    });
  }

  if (years) {
    addValueToContentFilter({
      key: 'years',
      values: years.split('|'),
    });
  }

  if (genres) {
    addValueToContentFilter({
      key: 'genres',
      values: genres.split('|'),
    });
  }

  if (translations) {
    addValueToContentFilter({
      key: 'translations',
      values: translations.split('|'),
    });
  }

  if (resolutions) {
    addValueToContentFilter({
      key: 'resolutions',
      values: resolutions.split('|'),
    });
  }

  if (certificates) {
    addValueToContentFilter({
      key: 'certificates',
      values: certificates.split('|'),
    });
  }

  if (ratings) {
    addValueToContentFilter({
      key: 'ratings',
      values: ratings.split('|'),
    });
  }
}

function initRouterHooks() {
  router.beforeEach((to, from, next) => {
    setMetaTitle(to.matched);

    const isAuthenticated = !!firebase.auth().currentUser;
    const isAuthOnlyArea = to.matched.some(record => record.meta.isAuthOnly);
    const isAnonOnlyArea = to.matched.some(record => record.meta.isAnonOnly);

    const isAnonAttemptsAccessToAuthOnlyArea = !isAuthenticated && isAuthOnlyArea;
    const isUserAttemptsAccessToAnonOnlyArea = isAuthenticated && isAnonOnlyArea;

    // console.log('-------------- beforeEach ------------------');
    // console.log('isAuthenticated', isAuthenticated);
    // console.log('isAuthOnlyArea', isAuthOnlyArea);
    // console.log('isAnonOnlyArea', isAnonOnlyArea);

    // console.log('--------------------------------');
    // console.log('isAnonAttemptsAccessToAuthOnlyArea ', isAnonAttemptsAccessToAuthOnlyArea );
    // console.log('isUserAttemptsAccessToAnonOnlyArea', isUserAttemptsAccessToAnonOnlyArea);
    // console.log('--------------------------------');

    if (isAnonAttemptsAccessToAuthOnlyArea ) {
      next({ name: 'auth-sign-in' });
    } else if (isUserAttemptsAccessToAnonOnlyArea) {
      next({ name: 'home' });
    } else {
      if (CONTENT_ROUTES.includes(to.name)) {
        setContentFilters(to.query);
      }

      next();
    }
  });

  isRouterHooksInited = true;
}

function closeAllPopups(event) {
  if((event.key=='Escape'||event.key=='Esc'||event.keyCode==27)){
    store.commit('ui/popups/CLOSE_ALL_POPUPS');
  }
}

window.addEventListener('keydown', closeAllPopups);

initTheme();

firebase.auth()
  .onAuthStateChanged(user => tryToAuth(user)
    .then(() => {
      if (!isRouterHooksInited) initRouterHooks();

      if (!app) {
        app = createApp(App);

        app.config.globalProperties.$_setTheme = setTheme;

        app.config.globalProperties.$_filters = {
          date,
          dateTime,
          duration,
          currency,
          number,
          country,
          genre,
          rating,
          resolution,
          translation,
          certificate,
          capitalize,
        };

        app
          // .use(VueMeta)
          .use(i18n)
          .use(store)
          .use(router)
          .mixin(globalMixin)
          .directive('click-outside', clickOutside)
          .mount('#app');
      }
    }));