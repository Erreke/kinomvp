import { createI18n } from 'vue-i18n';

import kz from './locales/kz.json';
import gb from './locales/gb.json';
import ru from './locales/ru.json';

let i18n;

if (!i18n) {
  i18n = createI18n({
    legacy: true,
    locale: 'kz',
    messages: {
      kz,
      gb,
      ru,
    }
  })
}

export default i18n;
