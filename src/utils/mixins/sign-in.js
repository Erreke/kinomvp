import { mapState, mapMutations, mapActions } from 'vuex';

import AppInputGroup from '@/views/blocks/input-group.vue';
import AppButton from '@/views/blocks/button.vue';
import AppButtonGroup from '@/views/blocks/button-group.vue';

export default {
  components: {
    AppInputGroup,
    AppButton,
    AppButtonGroup,
  },

  data() {
    return {
      email: '',
      password: '',
      isPasswordShowed: false,
    };
  },

  computed: {
    ...mapState('auth/sign-in', {
      formErrors: state => state.errors,
      isFormProcessing: state => state.isProcessing,
    }),

    ...mapState('auth/sign-up', {
      isSignedUpSuccessfully: state => state.isSuccess,
    }),

    ...mapState('auth/recover-password', {
      isRecoverPasswordShowed: state => state.isPopupOpened,
      isRecoverPasswordEmailSent: state => state.isSuccess,
    }),

    ...mapState('auth/recover-email', {
      isEmailRecoveredSuccessfully: state => state.isSuccess,
    }),

    ...mapState('auth/reset-password', {
      isPasswordResettedSuccessfully: state => state.isSuccess,
    }),

    ...mapState('auth/change-password', {
      isPasswordChangedSuccessfully: state => state.isSuccess,
    }),

    ...mapState('auth/email-verification', {
      isEmailVerifiedSuccessfully: state => state.isSuccess,
    }),

    ...mapState('auth/change-email', {
      isVerificationCodeSended: state => state.isSuccess,
    }),

    emailErrorMessages() {
      const errors = [];

      if (!this.v$.email.$dirty) return errors;

      if (this.v$.email.required.$invalid) errors.push(this.$t('errors.email-is-required'));
      if (this.v$.email.email.$invalid) errors.push(this.$t('errors.email-must-be-valid'));
      if (this.v$.email.minLength.$invalid) {
        errors.push(this.$t('errors.email-must-be-min-length', {
          length: this.v$.email.minLength.$params.min,
        }));
      }
      if (this.v$.email.maxLength.$invalid) {
        errors.push(this.$t('errors.email-must-be-max-length', {
          length: this.v$.email.maxLength.$params.max,
        }));
      }

      return errors;
    },

    passwordErrorMessages() {
      const errors = [];

      if (!this.v$.password.$dirty) return errors;
      if (this.v$.password.required.$invalid) errors.push(this.$t('errors.password-is-required'));
      if (this.v$.password.minLength.$invalid) {
        errors.push(this.$t('errors.password-must-be-min-length', {
          length: this.v$.password.minLength.$params.min,
        }));
      }
      if (this.v$.password.maxLength.$invalid) {
        errors.push(this.$t('errors.password-must-be-max-length', {
          length: this.v$.password.maxLength.$params.max,
        }));
      }

      return errors;
    },

    formErrorsMessages() {
      return this.formErrors.map(item => this.$t(`errors.sign-in.${item}`));
    },

    passwordType() {
      return this.isPasswordShowed ? 'text' : 'password';
    },

    passwordShowerIcon() {
      return this.isPasswordShowed ? 'm-0 fas fa-eye-slash' : 'm-0 fas fa-eye';
    },
  },

  methods: {
    ...mapActions('auth/sign-in', {
      signIn: 'SIGN_IN',
    }),

    ...mapMutations('auth/sign-in', {
      clearFormErrors: 'CLEAR_ALL_ERRORS',
    }),

    submitForm() {
      this.v$.$touch();

      if (!this.v$.$invalid) {
        this.signIn({
          email: this.email,
          password: this.password,
        })
          .then((user) => {
            if (user) {
              this.$router.push({
                name: 'home',
              });
            }
          });
      }
    },

    resetValidation(field) {
      this.clearFormErrors();
      this.v$[field].$reset();
    },

    redirectToSignUpPage() {
      this.$router.push({
        name: 'auth-sign-up'
      });
    },

    redirectToPasswordRecoverPage() {
      this.$router.push({
        name: 'auth-recover-password'
      });
    },
  },
};
