import { THEMES, PROJECT_NAME } from '@/config';

export default {
  computed: {
    $_screenWidth() {
      if (this.$root === this) {
        return this.screenWidth;
      }

      return this.$root.screenWidth;
    },

    $_device() {
      if (this.$root === this) {
        return this.device;
      }

      return this.$root.device;
    },

    $_user() {
      return this.$store.state.user.profile;
    },

    $_availableLocales() {
      return this.$i18n.availableLocales;
    },

    $_locale() {
      return this.$i18n.locale;
    },

    $_availableThemes() {
      return THEMES;
    },

    $_theme() {
      return 'light';
    },

    $_projectName() {
      return PROJECT_NAME;
    }
  },
};
