import i18n from '@/i18n';

export default function (value) {
  if (value === 'all') {
    return i18n.global.t('global.certificates.all');
  }

  return value;
}
