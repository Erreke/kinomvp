import i18n from '@/i18n';

export default function (values) {
  if (Array.isArray(values) && values.length) {
    const countries = values.map(country => {
      return i18n.global.t(`global.countries.${country}`);
    });

    return countries.join(', ');
  }

  return '';
}
