import i18n from '@/i18n';

export default function (value) {
  const hours = Math.floor(value / 60);
  const minutes = value % 60;

  return `${hours} ${i18n.global.t('global.filters.hours')} ${minutes} ${i18n.global.t('global.filters.minutes')}`;
}
