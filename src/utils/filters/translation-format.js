import i18n from '@/i18n';

export default function (values) {
  if (Array.isArray(values) && values.length) {
    const translations = values.map(translation => {
      return i18n.global.t(`global.translations.${translation}`);
    });

    return translations.join(', ');
  }

  return '';
}
