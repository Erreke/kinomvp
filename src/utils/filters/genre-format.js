import i18n from '@/i18n';

export default function (values) {
  if (Array.isArray(values) && values.length) {
    const genres = values.map(genre => {
      return i18n.global.t(`global.genres.${genre}`);
    });

    return genres.join(', ');
  }

  return '';
}
