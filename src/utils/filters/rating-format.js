export default function ({
  ratingOwn = 0,
  ratingImdb = 0,
  ratingRottentomatoes = 0,
  ratingMetacritic = 0,
  ratingKinopoisk = 0
}) {
  let ratingsSum = 0;
  let ratingsCount = 0;

  if (ratingOwn) { 
    ratingsCount += 1;
    ratingsSum += ratingOwn;
  }

  if (ratingImdb) {
    ratingsCount += 1;
    ratingsSum += ratingImdb;
  }

  if (ratingRottentomatoes) {
    ratingsCount += 1;
    ratingsSum += ratingRottentomatoes;
  }

  if (ratingMetacritic) {
    ratingsCount += 1;
    ratingsSum += ratingMetacritic;
  }

  if (ratingKinopoisk) {
    ratingsCount += 1;
    ratingsSum += ratingKinopoisk;
  }

  if (ratingsCount === 0) return ratingsCount;

  return Number(ratingsSum/ratingsCount).toFixed(1);
}
