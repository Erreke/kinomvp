import i18n from '@/i18n';

export default function (values) {
  if (Array.isArray(values) && values.length) {
    if (values.includes('4k')) {
      return i18n.global.t('global.resolutions.4k');
    } else if(values.includes('full-hd')) {
      return i18n.global.t('global.resolutions.full-hd');
    } else if(values.includes('hd')) {
      return i18n.global.t('global.resolutions.hd');
    }
  }

  return '';
}
