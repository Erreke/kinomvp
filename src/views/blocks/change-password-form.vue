<template>
  <form
    method="post"
    name="password-change"
    @submit.prevent="submitForm"
  >
    <input name="email" type="hidden" :value="$_user && $_user.email" />

    <app-input-group
      id="currentEmail"
      name="currentEmail"
      autocomplete="username"
      icon="fas fa-envelope"
      :label="$t('form.email.label')"
      :is-disabled="true"
      :value="currentEmail"
    />

    <app-input-group
      id="current-password"
      name="current-password"
      autocomplete="current-password"
      icon="fas fa-lock"
      :type="currentPasswordType"
      :label="$t('form.current-password.label')"
      :placeholder="$t('form.current-password.placeholder')"
      :is-disabled="isFormProcessing"
      :is-invalid="currentPasswordErrorMessages.length > 0"
      :has-right-button="true"
      :right-button-icon="currentPasswordShowerIcon"
      :invalid-texts="currentPasswordErrorMessages"
      v-model:value="currentPassword"
      @input:input="resetCurrentPasswordValidation"
      @click:right-button="isCurrentPasswordShowed = !isCurrentPasswordShowed"
    />

    <app-input-group
      id="new-password"
      name="new-password"
      autocomplete="new-password"
      icon="fas fa-lock"
      :type="newPasswordType"
      :label="$t('form.new-password.label')"
      :placeholder="$t('form.new-password.placeholder')"
      :is-disabled="isFormProcessing"
      :is-invalid="newPasswordErrorMessages.length > 0"
      :has-right-button="true"
      :right-button-icon="newPasswordShowerIcon"
      :invalid-texts="newPasswordErrorMessages"
      v-model:value="newPassword"
      @input:input="resetNewPasswordValidation"
      @click:right-button="isNewPasswordShowed = !isNewPasswordShowed"
    />

    <app-input-group
      id="newPasswordConfirm"
      name="newPasswordConfirm"
      autocomplete="new-password"
      icon="fas fa-lock"
      :type="newPasswordConfirmType"
      :label="$t('form.new-password-confirm.label')"
      :placeholder="$t('form.new-password-confirm.placeholder')"
      :is-disabled="isFormProcessing"
      :is-invalid="newPasswordConfirmErrorMessages.length > 0"
      :has-right-button="true"
      :right-button-icon="newPasswordConfirmShowerIcon"
      :invalid-texts="newPasswordConfirmErrorMessages"
      v-model:value="newPasswordConfirm"
      @input:input="resetNewPasswordConfirmValidation"
      @click:right-button="isNewPasswordConfirmShowed = !isNewPasswordConfirmShowed"
    />

    <div class="form-group">
      <div class="alert alert-danger" v-if="formErrorsMessages.length">
        <p v-for="(item, key) in formErrorsMessages" :key="key">{{ item }}</p>
      </div>
    </div>

    <app-button-group
      :is-primary-disabled="isFormProcessing"
      :is-secondary-disabled="isFormProcessing"
      :is-primary-loading="isFormProcessing"
      :primary-text="$t('block.change-password-dialog.change')"
      :secondary-text="$t('block.change-password-dialog.cancel')"
      @click:secondary="$emit('close-parent');"
    />
  </form>
</template>

<script>
import { mapState, mapActions, mapMutations } from 'vuex';
import useVuelidate from '@vuelidate/core';
import { required, minLength, maxLength, sameAs } from '@vuelidate/validators';
import weakPassword from '@/utils/validators/weak-password';
import AppInputGroup from '@/views/blocks/input-group.vue';
import AppButtonGroup from '@/views/blocks/button-group.vue';

const PASSWORD_MIN_LENGTH = 6;
const PASSWORD_MAX_LENGTH = 32;

export default {
  props: {
    canCloseParent: {
      type: Boolean,
      required: false,
      default: false,
    },
  },

  emits: [
    'password-changed',
    'keep-parent-opened',
    'close-parent',
  ],

  components: {
    AppInputGroup,
    AppButtonGroup,
  },

  data() {
    return {
      currentPassword: '',
      newPassword: '',
      newPasswordConfirm: '',
      isCurrentPasswordShowed: false,
      isNewPasswordShowed: false,
      isNewPasswordConfirmShowed: false,
    };
  },

  setup () {
    return { v$: useVuelidate() }
  },

  validations() {
    return {
      currentPassword: {
        required,
      },
      newPassword: {
        required,
        weakPassword,
        minLength: minLength(PASSWORD_MIN_LENGTH),
        maxLength: maxLength(PASSWORD_MAX_LENGTH),
      },
      newPasswordConfirm: {
        required,
        sameAsNewPassword: sameAs(this.newPassword),
      },
    };
  },

  computed: {
    ...mapState('auth/change-password', {
      formErrors: state => state.errors,
      isFormProcessing: state => state.isProcessing,
    }),

    currentEmail() {
      return this.$_user ? this.$_user.email : '';
    },

    currentPasswordErrorMessages() {
      const errors = [];

      if (!this.v$.currentPassword.$dirty) return errors;
      if (this.v$.currentPassword.required.$invalid) errors.push(this.$t('errors.change-password.current-password-is-required'));

      return errors;
    },

    newPasswordErrorMessages() {
      const errors = [];

      if (!this.v$.newPassword.$dirty) return errors;
      if (this.v$.newPassword.required.$invalid) errors.push(this.$t('errors.change-password.new-password-is-required'));
      if (this.v$.newPassword.weakPassword.$invalid) errors.push(this.$t('errors.change-password.new-password-is-weak'));
      if (this.v$.newPassword.minLength.$invalid) {
        errors.push(this.$t('errors.change-password.new-password-must-be-min-length', {
          length: this.v$.newPassword.minLength.$params.min,
        }));
      }
      if (this.v$.newPassword.maxLength.$invalid) {
        errors.push(this.$t('errors.change-password.new-password-must-be-max-length', {
          length: this.v$.newPassword.maxLength.$params.max,
        }));
      }

      return errors;
    },

    newPasswordConfirmErrorMessages() {
      const errors = [];

      if (!this.v$.newPasswordConfirm.$dirty) return errors;
      if (this.v$.newPasswordConfirm.required.$invalid) errors.push(this.$t('errors.change-password.new-password-confirm-is-required'));
      if (!this.v$.newPasswordConfirm.required.$invalid && this.v$.newPasswordConfirm.sameAsNewPassword.$invalid) errors.push(this.$t('errors.change-password.passwords-not-equal'));

      return errors;
    },

    formErrorsMessages() {
      return this.formErrors.map(item => this.$t(`errors.change-password.${item}`));
    },

    currentPasswordType() {
      return this.isCurrentPasswordShowed ? 'text' : 'password';
    },

    currentPasswordShowerIcon() {
      return this.isCurrentPasswordShowed ? 'm-0 fas fa-eye-slash' : 'm-0 fas fa-eye';
    },

    newPasswordType() {
      return this.isNewPasswordShowed ? 'text' : 'password';
    },

    newPasswordShowerIcon() {
      return this.isNewPasswordShowed ? 'm-0 fas fa-eye-slash' : 'm-0 fas fa-eye';
    },

    newPasswordConfirmType() {
      return this.isNewPasswordConfirmShowed ? 'text' : 'password';
    },

    newPasswordConfirmShowerIcon() {
      return this.isNewPasswordConfirmShowed ? 'm-0 fas fa-eye-slash' : 'm-0 fas fa-eye';
    },
  },

  methods: {
    ...mapActions('auth/change-password', {
      changePassword: 'CHANGE_USER_PASSWORD',
    }),

    ...mapMutations('auth/change-password', {
      clearFormErrors: 'CLEAR_ALL_ERRORS',
    }),

    async submitForm() {
      if (!this.isFormProcessing) {
        const isFormValid = await this.v$.$validate();

        if (isFormValid) {
          try {
            const isPasswordChanged = await this.changePassword({
              currentPassword: this.currentPassword,
              newPassword: this.newPassword,
            });

            if (isPasswordChanged) {
              this.$emit('password-changed');
            }
          } catch (error) {
            console.log('submitForm catch error');
            console.log('err:', error);
          }
        }
      }
    },

    resetCurrentPasswordValidation() {
      this.clearFormErrors();
      this.v$.currentPassword.$reset();
    },

    resetNewPasswordValidation() {
      this.clearFormErrors();
      this.v$.newPassword.$reset();
    },

    resetNewPasswordConfirmValidation() {
      this.clearFormErrors();
      this.v$.newPasswordConfirm.$reset();
    },
  },

  watch: {
    canCloseParent(value) {
      if (value && !this.isFormProcessing) {
        this.currentPassword = '';
        this.newPassword = '';
        this.newPasswordConfirm = '';
        this.isCurrentPasswordShowed = false;
        this.isNewPasswordShowed = false;
        this.isNewPasswordConfirmShowed = false;
        this.v$.$reset();
        this.$emit('close-parent');
      } else if (value) {
        this.$emit('keep-parent-opened');
      }
    }
  },
};
</script>