import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/pages/home/home.vue'
import NotFound from '../views/pages/not-found.vue'

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
      meta: {
        layout: 'default'
      }
    },
    {
      path: '/auth/sign-up/',
      name: 'auth-sign-up',
      component: () => import('../views/pages/auth/sign-up.vue'),
      meta: {
        layout: 'light',
        isAnonOnly: true,
      }
    },
    {
      path: '/auth/sign-in/',
      name: 'auth-sign-in',
      component: () => import('../views/pages/auth/sign-in.vue'),
      meta: {
        layout: 'light',
        isAnonOnly: true,
      }
    },
    {
      path: '/auth/reset-password/:code/',
      name: 'auth-reset-password',
      component: () => import('../views/pages/auth/reset-password.vue'),
      meta: {
        layout: 'light',
        isAnonOnly: true,
      }
    },
    {
      path: '/auth/recover-password/',
      name: 'auth-recover-password',
      component: () => import('../views/pages/auth/recover-password.vue'),
      meta: {
        layout: 'light',
        isAnonOnly: true,
      }
    },
    {
      path: '/profile/',
      name: 'profile',
      component: () => import('../views/pages/profile/profile.vue'),
      meta: {
        layout: 'default',
        isAuthOnly: true,
        breadcrumb: [
          'profile'
        ]
      }
    },
    {
      path: '/settings/',
      name: 'settings',
      component: () => import('../views/pages/settings/settings.vue'),
      meta: {
        layout: 'default',
        isAuthOnly: true,
        breadcrumb: [
          'settings'
        ]
      }
    },
    {
      path: '/payments/',
      name: 'payments',
      component: () => import('../views/pages/payments/payments.vue'),
      meta: {
        layout: 'default',
        isAuthOnly: true,
        breadcrumb: [
          'payments'
        ]
      }
    },
    {
      path: '/__/auth/action',
      name: 'auth-action',
      component: () => import('../views/pages/auth/action.vue'),
      meta: {
        layout: 'light',
      }
    },
    {
      path: '/results/',
      name: 'results',
      component: () => import('../views/pages/results/results.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'results'
        ]
      }
    },
    {
      path: '/new-arrivals/',
      name: 'new-arrivals',
      component: () => import('../views/pages/results/results.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'new-arrivals'
        ]
      }
    },
    {
      path: '/coming-soon/',
      name: 'coming-soon',
      component: () => import('../views/pages/movies/movies.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'coming-soon'
        ]
      },
    },
    {
      path: '/movies/',
      name: 'movies',
      component: () => import('../views/pages/movies/movies.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'movies'
        ]
      },
    },
    {
      path: '/movies/:id/',
      name: 'movies-item',
      component: () => import('../views/pages/movies/_id/movie.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'movies',
          '__self'
        ]
      }
    },
    {
      path: '/series/',
      name: 'series',
      component: () => import('../views/pages/series/series.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'series'
        ]
      }
    },
    {
      path: '/series/:id/',
      name: 'series-item',
      component: () => import('../views/pages/series/_id/series.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'series',
          '__self'
        ]
      }
    },
    {
      path: '/documentaries/',
      name: 'documentaries',
      component: () => import('../views/pages/documentaries/documentaries.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'documentaries'
        ]
      }
    },
    {
      path: '/documentaries/:id/',
      name: 'documentaries-item',
      component: () => import('../views/pages/documentaries/_id/documentary.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'documentaries',
          '__self'
        ]
      }
    },
    {
      path: '/cartoons/',
      name: 'cartoons',
      component: () => import('../views/pages/cartoons/cartoons.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'cartoons'
        ]
      }
    },
    {
      path: '/cartoons/:id/',
      name: 'cartoons-item',
      component: () => import('../views/pages/cartoons/_id/cartoon.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'cartoons',
          '__self'
        ]
      }
    },
    {
      path: '/collections/',
      name: 'collections',
      component: () => import('../views/pages/collections/collections.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'collections'
        ]
      }
    },
    {
      path: '/collections/:id/',
      name: 'collections-item',
      component: () => import('../views/pages/collections/_id/collection.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'collections',
          '__self'
        ]
      }
    },
    // {
    //   path: '/persons/',
    //   name: 'persons',
    //   component: () => import('../views/pages/persons/persons.vue'),
    //   meta: {
    //     layout: 'default',
    //     breadcrumb: [
    //       'persons'
    //     ]
    //   }
    // },
    // {
    //   path: '/persons/:id/',
    //   name: 'persons-item',
    //   component: () => import('../views/pages/persons/_id/person.vue'),
    //   meta: {
    //     layout: 'default',
    //     breadcrumb: [
    //       'persons',
    //       '__self'
    //     ]
    //   }
    // },
    {
      path: '/apps/',
      name: 'apps',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'apps'
        ]
      }
    },
    {
      path: '/apps/smart-tv/',
      name: 'apps-smart-tv',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'apps',
          'apps-smart-tv'
        ]
      }
    },
    {
      path: '/apps/ios/',
      name: 'apps-ios',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'apps',
          'apps-ios'
        ]
      }
    },
    {
      path: '/apps/android/',
      name: 'apps-android',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'apps',
          'apps-android'
        ]
      }
    },
    {
      path: '/promotions/',
      name: 'promotions',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'promotions',
        ]
      }
    },
    {
      path: '/code/',
      name: 'code',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'code',
        ]
      }
    },
    {
      path: '/faq/',
      name: 'faq',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'faq',
        ]
      }
    },
    {
      path: '/support/',
      name: 'support',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'support',
        ]
      }
    },
    {
      path: '/about-us/',
      name: 'about-us',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'about-us',
        ]
      }
    },
    {
      path: '/vacancies/',
      name: 'vacancies',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'vacancies',
        ]
      }
    },
    {
      path: '/advertising/',
      name: 'advertising',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'advertising',
        ]
      }
    },
    {
      path: '/referal-program/',
      name: 'referal-program',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'referal-program',
        ]
      }
    },
    {
      path: '/contacts/',
      name: 'contacts',
      component: () => import('../views/pages/construction.vue'),
      meta: {
        layout: 'default',
        breadcrumb: [
          'contacts',
        ]
      }
    },
    {
      path: '/:catchAll(.*)',
      name: 'not-found',
      component: NotFound,
      meta: {
        layout: 'default'
      }
    },
  ],
})
